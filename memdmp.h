//
// memdmp.h
// memdmp
//
// Copyright (c) 2013, Adam Knight
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
// 1. Redistributions of source code must retain the above copyright 
// notice, this list of conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright 
// notice, this list of conditions and the following disclaimer in the 
// documentation and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGE.

#ifndef _MEMDMP_H_
#define _MEMDMP_H_

#include <stdint.h>     // uint...
#include <stdio.h>      // FILE*

struct memdmp_opts {
    uint8_t base;               // Numberic base to use for encoded representation.
    struct {
        uint8_t width;          // Width of each encoded group
        uint8_t count;          // Number of encoded group per line
    } group;
    struct {
        int color;      // Use color
        int address;    // Show memory address
        int offset;     // Show offset in block
        int encoded;    // Show encoded dump
        int text;       // Show ASCII dump
        int padding;    // Show padding around each encoded pair
        int truncate;   // truncate blank lines
    } display;
};
typedef struct memdmp_opts memdmp_opts;

struct memdmp_state {
    // Last offset shown.
    off_t   offset;
    // The first 64 bytes of the previous line of input for truncation matching.
    char    prev[64];
    // The length of the previous line.
    size_t  nprev;
    // The truncation indicator has been shown; just increment the pos and skip.
    uint8_t truncated : 1;
};
typedef struct memdmp_state memdmp_state;

/**
 Print a block of memory to stdout in a stylized fashion, ASCII to the right.
 | 00000000 00000000 00000000 00000000 | ................ |
 (Above: four groups with width four)

 @param file File pointer to print to.
 @param data A buffer of data to print.
 @param length The number of bytes from the data pointer to print.
 @param opts An options structure. Pass NULL for defaults.
 @param state Provide a state structure if you will be making several consecutive and related calls (chunks of a file, for instance).  Offset and truncation state will be retained.
*/

ssize_t memdmp(FILE* file, const void* data, size_t length, const struct memdmp_opts* opts, struct memdmp_state* state);


/**
 Get a string representation of a block of memory in any reasonable number base.
 @param out A buffer with enough space to hold the output. Pass NULL to get the size to allocate.
 @param base The base to render the text as (2-36).
 @param data The buffer to render.
 @param length The size of the buffer.
 @return The number of characters written to *out (or that would be written), or -1 on error.
*/

ssize_t memstr(char* restrict out, uint8_t base, const void* data, size_t nbytes, size_t length);

#endif
