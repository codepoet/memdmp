//
// output.c
// memdmp
//
// Copyright (c) 2013, Adam Knight
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
// 1. Redistributions of source code must retain the above copyright 
// notice, this list of conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright 
// notice, this list of conditions and the following disclaimer in the 
// documentation and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGE.

#include <unistd.h>  // isatty, fileno
#include <stdlib.h>  // getenv
#include "output.h"

bool _useColor(FILE* f)
{
    return (getenv("NOCOLOR") == NULL ? isatty(STDOUT_FILENO) : false);
}

void _print_reset(FILE* f)
{
    if (_useColor(f) == false) return;
    fputs("\x1b[0m", f);
}

void _print_gray(FILE* f, uint8_t gray, bool background)
{
    if (_useColor(f) == false) return;
    // values must be from 0-23.
    int color = 232 + gray;
    fprintf(f, "\x1b[%s;5;%um", (background?"48":"38"), color);
}

void _print_color(FILE* f, uint8_t red, uint8_t green, uint8_t blue, bool background)
{
    if (_useColor(f) == false) return;
    // values must be from 0-5.
    if (red == green && green == blue) {
        _print_gray(f, red, background);
        return;
    }
    
    int color = 16 + (36 * red) + (6 * green) + blue;
    fprintf(f, "\x1b[%u;5;%um", (background?48:38), color);
}
